# MPCS 51031
## Weixiang Weng & Jingjie Wan
## TeamYelp

We built an app which can be used to search for imformation about business things via the Yelp Fusion API.

There are two dialogs on the searching page. To search information, you should firstly enter a term like "Chinese", "Indian" in the first dialog and enter a location like "5254 S Dorchester Avenue, Hyde Park, Chicago, IL" in the second dialog. Then you should choose a type which denotes the mode of presentation of the results. "Businese" means the app will return relatively complete information of ONE result that you search whereas "Phone" means it will return just the "name" and the "phone" of a list of results that you search. After you click the "SEARCH" button, it will jump into the result page.

Now you're in the result page. Please wait for a while because it may cost some time for the app to search. Then you can see results in the result table. If you choose the "Phone" searching type, you can click any single tuple in the table to access the related Yelp site of the tuple in your Browser. You may also click the "Return" button to return to the searching page.

You may also choose "exit" in the menu on the top right corner to close the app at any time. That's it, enjoy your searching!

## Slides

Just in the root of the repo.

## Reference

https://github.com/ranga543/yelp-fusion-android
