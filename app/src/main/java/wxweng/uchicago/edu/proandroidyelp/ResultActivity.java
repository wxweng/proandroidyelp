package wxweng.uchicago.edu.proandroidyelp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.yelp.fusion.client.connection.YelpFusionApi;
import com.yelp.fusion.client.connection.YelpFusionApiFactory;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.SearchResponse;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Math.min;

public class ResultActivity extends AppCompatActivity {

    private String API_Key;
    private String longitude;
    private String latitude;
    private Button retButton;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        //Set icon on the top
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.mipmap.ic_launcher);

        retButton = findViewById(R.id.return_button);
        retButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        type = intent.getIntExtra(YelpActivity.SEARCH_TYPE, 1);
        String terms = intent.getStringExtra(YelpActivity.SEARCH_TERM);
        String location = intent.getStringExtra(YelpActivity.SEARCH_LOC);

        update_location();
        API_Key = getKey("API_Key");

        new YelpSearchTask().execute(API_Key, terms, location, longitude, latitude);

    }

    private class YelpSearchTask extends AsyncTask<String, Void, SearchResponse> {
        private ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(ResultActivity.this);
            progressDialog.setTitle("Searching Result...");
            progressDialog.setMessage("One moment please...");
            progressDialog.setCancelable(true);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            YelpSearchTask.this.cancel(true);
                            progressDialog.dismiss();
                        }
                    });
            progressDialog.show();
        }

        @Override
        protected SearchResponse doInBackground(String... params) {

            String key          = params[0];
            String terms        = params[1];
            String location     = params[2];
            String longitude    = params[3];
            String latitude     = params[4];

            YelpFusionApiFactory apiFactory = new YelpFusionApiFactory();
            YelpFusionApi yelpFusionApi = null;
            try {
                yelpFusionApi = apiFactory.createAPI(key);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Map<String, String> search_params = new HashMap<>();

            // general params
            search_params.put("term", terms);
            if(location.isEmpty()) {
                search_params.put("latitude", latitude);
                search_params.put("longitude", longitude);
            } else {
                search_params.put("location", location);
            }

            Call<SearchResponse> call = yelpFusionApi.getBusinessSearch(search_params);
            try {
                Response<SearchResponse> response = call.execute();
                return response.body();
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(SearchResponse response) {
            // Update UI Text
            if (type == 2) {
                // Show phone results
                int totalNumofResult = response.getTotal();
                int listnum = min(totalNumofResult, 6);
                ArrayList<Business> businesses = response.getBusinesses();
                TableLayout table = findViewById(R.id.my_table);


                for (int i = 0; i < listnum; i++) {
                    Business business = businesses.get(i);

                    LayoutInflater inflater = LayoutInflater.from(ResultActivity.this);
                    TableRow row = (TableRow) inflater.inflate(R.layout.result_row, null, false);

                    String name = business.getName();
                    String phone = business.getPhone();
                    final String url = business.getUrl();

                    TextView row_item = row.findViewById(R.id.my_item);
                    TextView row_content = row.findViewById(R.id.my_content);
                    row_item.setText(name);
                    row_content.setText(phone);

                    // Set a url which can be launched in Browser when clicked
                    row.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(browserIntent);
                        }
                    });
                    table.addView(row);
                }

            } else {
                // Show general business results
                ArrayList<Business> businesses = response.getBusinesses();
                TableLayout table = findViewById(R.id.my_table);

                Business business = businesses.get(0);

                ArrayList<String> items = new ArrayList<>();
                String name = "Name," + business.getName();
                String phone = "Phone," + business.getPhone();
                String distance = "Distance," + Double.toString(business.getDistance());
                String rating = "Rating," + Double.toString(business.getRating());
                Boolean isClosed = business.getIsClosed();
                String isOpen = "IsOpen,";
                if (isClosed)
                    isOpen += "No";
                else isOpen += "Yes";

                items.add(name);
                items.add(phone);
                items.add(isOpen);
                items.add(distance);
                items.add(rating);

                for (int i = 0; i < 5; ++i) {
                    LayoutInflater inflater = LayoutInflater.from(ResultActivity.this);
                    TableRow row = (TableRow) inflater.inflate(R.layout.result_row, null, false);
                    
                    TextView row_item = row.findViewById(R.id.my_item);
                    TextView row_content = row.findViewById(R.id.my_content);
                    String strs[] = items.get(i).split(",");
                    String item = strs[0];
                    String content = strs[1];
                    row_item.setText(item);
                    row_content.setText(content);

                    table.addView(row);
                }
            }
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //For the menu of exit
        int id = item.getItemId();

        switch (id) {
            case R.id.action_exit:
                finish();
                return true;
            default:
                return false;
        }

    }

    private String getKey(String keyName){
        AssetManager assetManager = this.getResources().getAssets();
        Properties properties = new Properties();
        try {
            InputStream inputStream = assetManager.open("keys.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  properties.getProperty(keyName);
    }

    private void update_location() {
        // TODO: get longitude and latitude from phone
        // just for test the location
        // longitude and latitude at San Francisco
        latitude = "40.581140";
        longitude = "-111.914184";

    }

}
