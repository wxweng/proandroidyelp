package wxweng.uchicago.edu.proandroidyelp;

import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.yelp.fusion.client.connection.YelpFusionApi;
import com.yelp.fusion.client.connection.YelpFusionApiFactory;
import com.yelp.fusion.client.models.SearchResponse;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YelpActivity extends AppCompatActivity {

    private EditText searchTerm;
    private EditText searchLocation;
    private Button searchButton;
    private int searchType;

    public static final String SEARCH_TYPE = "TYPE";
    public static final String SEARCH_TERM = "TERM";
    public static final String SEARCH_LOC = "LOCATION";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yelp);

        searchTerm = findViewById(R.id.edit_search_term);
        searchLocation = findViewById(R.id.edit_search_location);
        searchButton = findViewById(R.id.search_button);
        searchType = 0;

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String terms = searchTerm.getText().toString();
                String location = searchLocation.getText().toString();
                if(searchType == 0 || terms.length() == 0) {
                    Toast.makeText(YelpActivity.this, "Type not selected or no search term, try again.", Toast.LENGTH_LONG).show();
                } else {
                    searchResult(searchType, terms, location);
                }
            }
        });

        //Set icon on the top
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.mipmap.ic_launcher);
    }

    private void searchResult(int type, String terms, String location) {
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra(SEARCH_TYPE, type);
        intent.putExtra(SEARCH_TERM, terms);
        intent.putExtra(SEARCH_LOC, location);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //For the menu of exit
        int id = item.getItemId();

        switch (id) {
            case R.id.action_exit:
                finish();
                return true;
            default:
                return false;
        }

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_button1:
                if (checked)
                    searchType = 1;
                break;
            case R.id.radio_button2:
                if (checked)
                    searchType = 2;
                break;
        }
    }

}
